FROM d4science/gcore-distribution

RUN wget --no-check-certificate https://nexus.d4science.org/nexus/content/repositories/gcube-releases/org/gcube/resourcemanagement/resource-manager-service/2.2.1-4.3.0-142590/resource-manager-service-2.2.1-4.3.0-142590-full.gar && gcore-deploy-service resource-manager-service-2.2.1-4.3.0-142590-full.gar

RUN rm /home/gcube/gCore/lib/registry-publisher-1.2.6-4.3.0-126697.jar
COPY --chown=gcube:gcube src/registry-publisher-1.3.1-4.16.0-183497.jar /home/gcube/gCore/lib/

RUN mkdir $HOME/.gcore && chown gcube:gcube $HOME/.gcore
RUN mkdir -p /gcube-data/persisted
RUN ln -s /gcube-data/persisted $HOME/.gcore/