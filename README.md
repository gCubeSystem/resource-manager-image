# Resource Manager Service Docker image

This repo contains the Dockerfile to build the image of the gCore Resource Manager service

## Build the image

```shell
$ docker build -t resource-manager-image .
```

## Test the image

```shell
$ docker container run --name d4science/gcore-resource-manager resource-mamager-image 
```
